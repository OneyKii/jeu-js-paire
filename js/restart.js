// function restart() + grab sur le bouton 'nouvelle partie'
function buttonRestart() {
    button = document.getElementById('button');
    // ajout d'un écouteur d'évenement sur 'button'
    button.addEventListener("click", function () {
        // redirection sur 'index.html'
        window.location.href = "index.html";
    })
}
// éxecution de la function restart()
buttonRestart();