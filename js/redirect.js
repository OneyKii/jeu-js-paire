// function redirectToClassement() + grab sur le bouton 'Dernier Score'
function redirectToClassement() {
    lastScore = document.getElementById('lastScore');
    // ajout d'un écouteur d'évenement sur 'lastScore'
    lastScore.addEventListener("click", function () {
        // redirection sur 'classement.html'
        window.location.href = "classement.html";
    })
}

redirectToClassement();