/* 
    Directive et idée de méthod pris de ce tuto
    Code globalement modifier et repenser
    https://www.bonbache.fr/le-jeu-internet-du-memory-par-le-code-javascript-285.html
*/


// Création des variables pour pouvoir les utilisés après

var nbrClick = 0;
var imgInMini1 = "";
var imgInMini2 = "";
var case1 = "";
var case2 = "";
var imgOk = 0;
var nbrError = 0;
var scoreGlobal = 0;
var start = false;
// récupération de la Date actuel
var startTime = new Date().getTime();

// appel de function generation()
generation();

// Funtion permettant la visibilité des cartes 
// pendant 2 secondes avant que la partie commence. 
var attente = setTimeout(function () {
    for (var i = 0; i < 16; i++) {
        document.getElementById('img' + i).src = "mini/miniz.png";
    }
    start = true;
}, 2000);

// Commencement de la function generation()
function generation() {
    // appels des variables crées au préalable
    let nbrAleatoire;
    let nbrImage = "";
    let test = true;
    let chaine = "";

    // for pour lister toute les images (mini(nbr générer).png)
    for (var i = 0; i < 16; i++) {
        // tant que test = true continuer de suivre la procédure
        while (test == true) {
            // génération des 16 chiffres pour les images qu'on imprimera un peu après
            nbrAleatoire = Math.floor(Math.random() * 16) + 1;
            // si chaine n'est pas égale à 
            // renvoie vers la première information (normalement utilisé dans un tableau)
            if (chaine.indexOf("-" + nbrAleatoire + "-") > -1) {
                // génère les nombres aléatoires comme l.50
                nbrAleatoire = Math.floor(Math.random() * 16) + 1;
            }
            else {
                // alors mettre le nombre d'image (16) / 2 pour faire les paires (1-8)
                nbrImage = Math.floor((nbrAleatoire + 1) / 2);
                // récuperation de case + (chiffre entre 0 & 15) 
                // ajout des images (8 paires pour 16 cases)
                document.getElementById('case' + i).innerHTML = "<img style='cursor:pointer;' id='img" + i + "' src='mini/mini" + nbrImage + ".png' onClick='verifier(\"img" + i + "\",\"mini" + nbrImage + "\")' alt='' />";
                chaine += "-" + nbrAleatoire + "-";
                test = false;
            }
        }
        test = true;
    }
}

// appels de la function verifier() en amont
verifier();

// commencement de la function verifier()
function verifier(limg, source) {
    // si tout est ok pour commencer
    if (start == true) {
        // nbrClick = 1 pour pouvoir faire des paires
        nbrClick++;
        // récupère l'id de l'imgage et créer l'image
        // + r'ajouter les chiffres de 1 à 16.
        document.getElementById(limg).src = "mini/" + source + ".png";
        // condition pour cliquer uniquement 2x sur 2 img différente
        // et pouvoir permettre la selection de celle-ci
        if (nbrClick == 1) {
            imgInMini1 = source;
            case1 = limg;
        } else {
            imgInMini2 = source;
            case2 = limg;


            if (case1 != case2) {
                start = false;
                if (imgInMini1 != imgInMini2) {
                    // création de la function pour que les images se retourne
                    // quand elle ne sont pas les mêmes
                    var attente = setTimeout(function () {
                        // récupération dans l'HTML des cases retournés
                        document.getElementById(case1).src = "mini/miniz.png";
                        document.getElementById(case2).src = "mini/miniz.png";
                        start = true;
                        nbrClick = 0;
                        nbrError++;
                        // condition si le joueur dépasse 10 erreurs
                        // que le jeu s'arrête
                        if (nbrError < 11) scoreGlobal = 10 - nbrError;
                        document.getElementById("score").innerHTML = "<strong>" + scoreGlobal + "</strong>/10";
                    }, 1000);
                } else {
                    start = true;
                    nbrClick = 0;
                    imgOk += 2;
                    if (imgOk == 16) {
                        var timeScore = Math.floor((new Date().getTime() - startTime) / 1000);
                        // récupération du score & time
                        // donne un score /10 en déduisant le nbr de faute
                        // + donne le time que le joueurs à mis pour faire le jeu
                        document.getElementById("score").innerHTML = scoreGlobal + " / 10";
                        document.getElementById("time").innerHTML = "Tu l'as fais en " + timeScore + " secondes";

                        localStorage.setItem('temps', timeScore);
                    }

                    // si le joueur à faire + de 180s
                    // arrêt du jeu et ajout d'un message car il est trop nul x)
                    if (timeScore > 180) {
                        document.getElementById("time").innerHTML = "BAHAHAHAH MEME EN TROIS MINUTES TU AS PAS REUSSI(E)";
                        start = false;
                    }
                }
            } else {
                if (nbrClick == 2) nbrClick = 1;
            }
        }
    }
}

// function restart() + grab sur le bouton 'nouvelle partie'
function buttonRestart() {
    button = document.getElementById('button');
    // ajout d'un écouteur d'évenement sur 'button'
    button.addEventListener("click", function () {
        // redirection sur 'index.html'
        window.location.href = "index.html";
    })
}
// éxecution de la function restart()
buttonRestart();
